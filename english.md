[Zur deutschen Beschreibung](/channelrole/)

> Have you ever thought of an alternative soloution to reaction Roles on Discord?
Well, I did! And my soloution: ChannelRoles!

## What's a ChannelRole?
ChannelRole means, there's a Voicechannel, visible for everyone. If a Member enters the channel, he will get the defined role and will be kicked out of the channel. Now, the Member will be displayed a Channel for removing from the Role.
If the User enters this channel, the role will be taken from the Member.

## Which commands can I use?
### cr!create <@Role>
You can use the cr!create <@Role>-Command in order to create a new ChannelRole. Just type the command and replace <@Role> with a mention of the role, you want to create a reactionRole from.

### cr!invite
You can use this command in order to get an Invite-Link for the Bot or in order to visit my Discord-Server

### cr!help
Using this command, you will recieve a german help-message for the bot.

## May I rename the Channels or the Category?
Yes, easily rename those as you like. But: Don't remove them! This will delet your Channelrole!

## What do I have to do in order to delete a ChannelRole?
Nothing special. Easily delete both channels and the category and the channelRole is removed from the System.

Thanks for you interest in the bot. If you like it, I'm happy about a vote on top.gg :D
