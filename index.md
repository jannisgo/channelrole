[Click here for the english documentaion](english)

> Hast du schon einmal über Alternativen zur ReactionRole nachgedacht?
Ich schon, deshalb kam ich auf die Idee der ChannelRole!

## Was ist die ChannelRole?
ChannelRole bedeutet, dass es einen Channel gibt, der für alle sichtbar ist und die User einfach in den VoiceChannel gehen, automatisch aus dem Channel gekickt werden und dem Nutzer die vermerkte Rolle zugeteilt wird.
Betritt der Nutzer den Channel zur Deaktivierung, wird die Rolle wieder entfernt!

## Welche Befehle gibt es?
### cr!create <@Rolle>
Mit dem cr!create-Befehl kannst du eine neue ChannelRole erstellen. Schreibe dafür einfach den Befehl und dahinter eine Mention der Rolle, für die die ChannelRole erstellt werden soll.
Automatisch werden eine Kategorie und die nötigen Channels erstellt.

### cr!invite
Mit diesem Befehl erhältst du einen Einladungslink für den Bot und den Discord-Server von mir.

### cr!help
Der Befehl cr!help schickt dir eine Nachricht mit Hilfestellungen, falls etwas nicht funktioniert.

## Kann ich die Kanäle oder die Kategorie umbenennen?
Ja, benenne einfach wie gewohnt Kategorie oder Channel um. (Das geht, da die Channels anhand der ID gespeichert sind.)

## Was muss ich tun, um die Kanäle zu löschen?
Nichts besonderes. Lösche einfach die Kanäle - sie werden automatisch aus dem System entfernet.

Vielen Dank, dass du dich für meinen Bot interessierst!
